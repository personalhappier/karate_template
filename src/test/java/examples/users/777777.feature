@vasya
Feature: Create a new post using POST request

  Background:
    # Setting the base URL for the API
    * url 'https://jsonplaceholder.typicode.com'

  Scenario: Create a new post
    Given url 'https://jsonplaceholder.typicode.com/posts'
    And request { "title": "foo", "body": "bar", "userId": 1 }
    When method post
    Then status 201
    And match response == { "id": "#number", "title": "foo", "body": "bar", "userId": 1 }

  Scenario: 111111111111111111111111 get the first user by id
    Given path 'users'
    When method get
    Then status 200

    * def first = response[0]

    Given path 'users', first.id
    When method get
    Then status 200