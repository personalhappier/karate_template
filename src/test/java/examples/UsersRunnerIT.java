package examples;

import com.intuit.karate.junit5.Karate;

class UsersRunnerIT {
    
    @Karate.Test
    Karate testUsers() {
        return Karate.run("classpath:examples/users",
                                 "classpath:examples/customer");
    }

//    @Karate.Test
//    Karate testCustomer() {
//        return Karate.run("classpath:examples/customer");
//    }

}
